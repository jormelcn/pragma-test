function createEditTaskManager(template){

  var editId = null;
  var newAssignedListener = null;
  var emptyAssignedListener = null;
  var assignedContainer = null;
  var userAccess = null;


  function validateDate(campo) {
    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if ((campo.match(RegExPattern)) && (campo!='')) {
      var fechaf = campo.split("/");
      var day = fechaf[0];
      var month = fechaf[1];
      var year = fechaf[2];
      var date = new Date(year, month, '0');
      if((day-0)>(date.getDate()-0)){
        return false;
      }
      return true;
    } else {
        return false;
    }
  }

  function validateNewDate(date){
    var x=new Date();
    var fecha = date.split("/");
    x.setFullYear(fecha[2],fecha[1]-1,fecha[0]);
    var today = new Date();
    if (x >= today)
      return true;
    else
      return false;
  }

  function addNewAssignedInput(){
    assignedContainer.lastChild.removeEventListener('keydown', newAssignedListener);
    assignedContainer.lastChild.addEventListener('keydown', emptyAssignedListener);
    var newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Agrega un responsable...'
    assignedContainer.appendChild(newInput);
    newInput.addEventListener('keydown', newAssignedListener);
  }

  function removeAssignedInput(input){
    input.removeEventListener('keyDown', emptyAssignedListener);
    assignedContainer.removeChild(input);
    assignedContainer.lastChild.focus();
  }

  function initAssignedManager(){
    assignedContainer = document.getElementById('edit-task:form:assigned-container');
    assignedContainer.innerHTML = ' ';
    newAssignedListener = function(evt){
      if(evt.target.value && assignedContainer.childNodes.length < 5){
        addNewAssignedInput();
      }
    }
    emptyAssignedListener = function(evt){
      if(!evt.target.value){
        if(assignedContainer.lastChild.value){
          addNewAssignedInput();
        }
        removeAssignedInput(evt.target);
      }
    }
    addNewAssignedInput();
  }

  function getAssignedList(){
    var assigned = [];
    for(var i = 0; i < assignedContainer.childNodes.length; i++){
      if(
        assignedContainer.childNodes[i] instanceof HTMLInputElement &&
        assignedContainer.childNodes[i].value
      ){
        assigned.push(assignedContainer.childNodes[i].value);
      }
    }
    return assigned;
  }

  function saveTask(task){
    Database.saveNewTask(userAccess.token, task, function(info){
      try{
        alert('tarea guardada');
        Router.goToSpace('tasks-pending');
      }catch(e){}
    }, function(){
      //ERROR
    });
  }

  function updateTask(task){
    Database.updatePendingTask(userAccess.token, editId, task, function(){
      try{
        alert('Cambios guardados');
      }catch(e){}
    }, function(){
      //ERROR
    })
  }

  function validateTask(task){
    if(!validateDate(task.date)){
      alert('Ingrese una fecha correcta');
      return false;
    }
    if(!validateNewDate(task.date)){
      alert('La fecha introducida ya pasó');
      return false;
    }
    return true;
  }

  function initFormManager(){
    initAssignedManager();
    var form = document.getElementById('edit-task:form');
    form.reset();
    form.addEventListener('submit', function(evt){
      evt.preventDefault();
      var task = {
        name : form.elements['name'].value,
        date : form.elements['date'].value,
        description : form.elements['description'].value,
        assigned : getAssignedList()
      };
      if(validateTask(task)){
        if(editId) updateTask(task);
        else saveTask(task);
      }
    });
  }

  function setFormDisabled(disabled){
    //TODO
  }

  function populateFormForEdit(){
    Database.getTask(userAccess.token, editId, function(task){
      try{
        var form = document.getElementById('edit-task:form');
        form.elements['name'].value = task.name;
        form.elements['description'].value = task.description;
        form.elements['date'].value = task.date;
        for(var i = 0; i < task.assigned.length; i++){
          assignedContainer.lastChild.value = task.assigned[i];
          if(assignedContainer.childNodes.length < 5)
            addNewAssignedInput();
        }
        setFormDisabled(false);
      }catch(e){}
    }, function(){
      //Error
    });
  }

  function registerAuthListener(){
    authListener = function(access){
      userAccess = access;
      if(access){
        if(editId){
          populateFormForEdit();
        }else{
          setFormDisabled(false);
        }
      }
      else{
        //disable Form
        SignIn.requestAuth();
      }
    };
    Auth.onAuthChange(authListener);
  }

  function EditTaskManager(template){
    
    this.template = template;
    
    this.load = function(args){
      if(args){
        editId = args.id;
        document.getElementById('edit-task:title').innerHTML = "Editar Tarea:"
      }
      initFormManager();
      registerAuthListener();
    };

    this.destroy = function(){
      editId = null;
      Auth.removeAuthListener(authListener);
    };

  }

  return new EditTaskManager(template);
}

EditTask = createEditTaskManager('spaces/edit-task/edit-task.html');