function createTasksPendingManager(template){

  var authListener = null;
  var tasksClickListener = null;
  var taskTemplate = null;
  var tasksContainer = null;
  var userAccess = null;
  var onMoreProgress = false;
  var findKey = null;
  var listCount = 0;

  function renderTask(task){
    var taskUI = document.createElement('div');
    taskUI.classList.add('task');
    taskUI.id = "tasks-pending:task:" + task.id;
    taskUI.innerHTML = Mustache.render(taskTemplate, task);
    return taskUI;
  }

  function addTasks(tasks){
    for(var i = 0; i < tasks.length; i++){
      listCount++;
      tasksContainer.appendChild(renderTask(tasks[i]));
    }
  };

  function setTasks(tasks){
    tasksContainer.innerHTML = '';
    listCount = 0;
    addTasks(tasks);
    if(tasks.length == 10){
      enableMoreButton();
    }
    else{
      disableMoreButton();
    }
  }

  function populateTasks(){
    Database.getPendingTasks(userAccess.token, 0, 10, function(tasks){
      try{
        findKey = null;
        setTasks(tasks);
      }catch(e){}
    }, function(){
      //ERROR
    });
  }

  function findTasks(key){
    Database.findPendingTasks(userAccess.token, key, 0, 10, function(tasks){
      try{
        findKey = key;
        setTasks(tasks);
      }catch(e){}
    }, function(){
      //ERROR
    });
  }

  function loadTaskTemplate(){
    taskTemplate = document.getElementById('tasks-pending:task-template').innerHTML;
  }

  function registerAuthListener(){
    authListener = function(access){
      userAccess = access;
      if(access){
        populateTasks(access);
      }
      else{
        SignIn.requestAuth();
      }
    };
    Auth.onAuthChange(authListener);
  }

  function showTaskDetail(id){
    alert('ver detalle: ' + id);
  }

  function completeTask(id){
    Database.setTaskCompleted(userAccess.token, id, function(){
      try{
        var element = document.getElementById('tasks-pending:task:' + id);
        element.id = '';
        element.classList.add('removed-item');
        listCount--;
        setTimeout(function(){
          tasksContainer.removeChild(element);
        }, 200);
      }catch(e){}
    }, function(){
      //Error
    });
  }

  function registerClickListeners(){
    tasksClickListener = function(evt){
      var id = evt.target.id.split(':');
      if(id.length >= 3 && id[0] == 'tasks-pending' && id[1] == 'task'){
        if(id.length == 3) Router.goToSpace('task-detail',{id : id[2]});
        if(id.length == 4 && id[3] == 'completed' && confirm('Desea marcar esta tarea como completada?')) completeTask(id[2]);
        if(id.length == 4 && id[3] == 'edit') Router.goToSpace('edit-task',{id : id[2]});
        if(id.length == 4 && id[3] == 'name') Router.goToSpace('task-detail',{id : id[2]});
      }
    };
    tasksContainer.addEventListener('click', tasksClickListener);
  }

  function initFindForm(){
    var form = document.getElementById('tasks-pending:find-form');
    form.addEventListener('submit', function(evt){
      evt.preventDefault();
      findTasks(form.elements['key'].value);
    });
    form.elements['key'].addEventListener('keyup', function(evt){
      if(!evt.target.value){
        populateTasks();
      }
    });
  }

  function initNewTaskButton(){
    var newTaskButton = document.getElementById('tasks-pending:new-task-button');
    newTaskButton.addEventListener('click', function(){
      Router.goToSpace('edit-task');
    });
  }

  function addMoreTasksToList(){
    if(findKey){
      Database.findPendingTasks(userAccess.token, findKey, listCount, 5, function(tasks){
        try{
          addTasks(tasks);
          var button = document.getElementById('tasks-pending:more-button');
          button.classList.remove('loading');
        }catch(e){}
      }, function(){
        alert();
        //ERROR
      });
    }
    else{
      Database.getPendingTasks(userAccess.token, listCount, 5, function(tasks){
        try{
          addTasks(tasks);
          var button = document.getElementById('tasks-pending:more-button');
          button.classList.remove('loading');
        }catch(e){}
      }, function(){
        alert();
        //ERROR
      });
    }
  }

  function initMoreButton(){
    var button = document.getElementById('tasks-pending:more-button');
    button.addEventListener('click', function(evt){
      if(!onMoreProgress)
        button.classList.add('loading');
        addMoreTasksToList();
    });
  }

  function enableMoreButton(){
    var button = document.getElementById('tasks-pending:more-button');
    button.style.display = 'inline-block';
  }

  function disableMoreButton(){
    var button = document.getElementById('tasks-pending:more-button');
    button.style.display = 'none';
  }

  var Manager = function(template){
    
    this.template = template;
    
    this.load = function(){
      tasksContainer = document.getElementById('tasks-pending:tasks-container');
      loadTaskTemplate();
      registerAuthListener();
      registerClickListeners();
      initFindForm();
      initNewTaskButton();
      initMoreButton();
    };

    this.destroy = function(){
      Auth.removeAuthListener(authListener);
      authListener = null;
      taskTemplate = null;
      tasksContainer = null;
    }
  }
  return new Manager(template);
}

TasksPending = createTasksPendingManager('spaces/tasks-pending/tasks-pending.html');