function createTaskDetailManager(template){

  var id = 0;
  var userAccess = null;

  function populateDetail(){
    Database.getTask(userAccess.token, id, function(task){
      try{
        var template = document.getElementById('task-detail:template').innerHTML;
        var container = document.getElementById('task-detail:container');
        container.innerHTML = Mustache.render(template, task);
        initEditButton();
      }catch(e){}
    }, function(){
      //ERROR
    });
  }

  function registerAuthListener(){
    authListener = function(access){
      userAccess = access;
      if(access){
        populateDetail();
      }
      else{
        //disable Form
        SignIn.requestAuth();
      }
    };
    Auth.onAuthChange(authListener);
  }

  function initEditButton(){
    var button = document.getElementById('task-detail:edit-task-button');
    button.addEventListener('click', function(evt){
      Router.goToSpace('edit-task',{id : id});
    });
  }
  
  function Manager(template){
    this.template = template;

    this.load = function(args){
      id = args.id;
      registerAuthListener();   
    };

    this.destroy = function(){
      Auth.removeAuthListener(authListener);
    };
  }
  return new Manager(template);
}

TaskDetail = createTaskDetailManager('spaces/task-detail/task-detail.html');