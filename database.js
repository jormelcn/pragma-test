function createDatabaseManager(){

  var _token = "hjhshd871231319bjasdad71";

  var desc = 'Esta tarea debe ser realizada lo antes posible';
  var n1 = 'Jorge Narvaez Cavadia';
  var n2 = 'Rafael Pombo';
  var n3 = 'Rene Descartes';
  var n4 = 'Nicola Tesla';

  var pendingTasks = [
    { id : 1, date: '15/02/2018', name: 'Cortar del cesped', description: desc, assigned : [n1, n2], completed: false},
    { id : 2, date: '15/02/2018', name: 'Regar las plantas del Jardin', description: desc, assigned : [n2, n3], completed: false},
    { id : 3, date: '15/02/2018', name: 'Hacer Mercado', description: desc, assigned : [n3, n4], completed: false},
    { id : 4, date: '15/02/2018', name: 'Armar el nuevo escritorio', description: desc, assigned : [n4, n1, n2], completed: false},
    { id : 5, date: '15/02/2018', name: 'Instalar la luz del patio', description: desc, assigned : [n1, n2, n3], completed: false},
    { id : 6, date: '15/02/2018', name: 'Pasear el perro y darle de comer su comida favorita', description: desc, assigned : [n2, n3, n4], completed: false},
    { id : 7, date: '15/02/2018', name: 'Enviar el informe', description: desc, assigned : [n1, n2, n3, n4], completed: false},
    { id : 8, date: '15/02/2018', name: 'Hacer Mercado', description: desc, assigned : [n3, n4], completed: false},
    { id : 9, date: '15/02/2018', name: 'Armar el nuevo escritorio', description: desc, assigned : [n4, n1, n2], completed: false},
    { id : 10, date: '15/02/2018', name: 'Instalar la luz del patio', description: desc, assigned : [n1, n2, n3], completed: false},
    { id : 11, date: '15/02/2018', name: 'Pasear el perro y darle de comer su comida favorita', description: desc, assigned : [n2, n3, n4], completed: false},
    /*{ id : 12, date: '15/02/2018', name: 'Enviar el informe', description: desc, assigned : [n1, n2, n3, n4], completed: false},
    { id : 13, date: '15/02/2018', name: 'Hacer Mercado', description: desc, assigned : [n3, n4], completed: false},
    { id : 14, date: '15/02/2018', name: 'Armar el nuevo escritorio', description: desc, assigned : [n4, n1, n2], completed: false},
    { id : 15, date: '15/02/2018', name: 'Instalar la luz del patio', description: desc, assigned : [n1, n2, n3], completed: false},
    { id : 16, date: '15/02/2018', name: 'Pasear el perro y darle de comer su comida favorita', description: desc, assigned : [n2, n3, n4], completed: false},
    { id : 17, date: '15/02/2018', name: 'Enviar el informe', description: desc, assigned : [n1, n2, n3, n4], completed: false},
    { id : 18, date: '15/02/2018', name: 'Hacer Mercado', description: desc, assigned : [n3, n4], completed: false},
    { id : 19, date: '15/02/2018', name: 'Armar el nuevo escritorio', description: desc, assigned : [n4, n1, n2], completed: false},
    { id : 20, date: '15/02/2018', name: 'Instalar la luz del patio', description: desc, assigned : [n1, n2, n3], completed: false},
    { id : 21, date: '15/02/2018', name: 'Pasear el perro y darle de comer su comida favorita', description: desc, assigned : [n2, n3, n4], completed: false},
    { id : 22, date: '15/02/2018', name: 'Enviar el informe', description: desc, assigned : [n1, n2, n3, n4], completed: false},*/
  ];

  var completedTasks = [
    { id : 100, name: 'Recojer la basura', description: desc, assigned : [n3, n4], completed: true},
    { id : 101, name: 'Fotocopiar los documentos importantes', description: desc, assigned : [n4, n1, n2], completed: true},
    { id : 102, name: 'Recojer la basura', description: desc, assigned : [n3, n4], completed: true},
    { id : 103, name: 'Fotocopiar los documentos importantes', description: desc, assigned : [n4, n1, n2], completed: true},
    { id : 104, name: 'Recojer la basura', description: desc, assigned : [n3, n4], completed: true},
    { id : 105, name: 'Fotocopiar los documentos importantes', description: desc, assigned : [n4, n1, n2], completed: true},
  ];


  function getTasksOfList(list, offset, limit){
    var result = [];
    for(var i = offset; i < offset + limit && i < list.length; i++)
      result.push(list[i]);
    return result;
  }

  function findTasksOnList(list, key, offset, limit){
    var normalKey = key.toLowerCase();
    var findResult = [];
    for(var i = 0; i < list.length; i++){
      var task = list[i];
      if(
        task.name.toLowerCase().indexOf(normalKey) != -1 || 
        task.description.toLowerCase().indexOf(normalKey) != -1 ||
        task.assigned.join().toLowerCase().indexOf(normalKey) != -1
      ){
        findResult.push(task);
      }
    }
    findResult.splice(0, offset);
    if(findResult.length > limit){
      findResult.splice(limit);
    }
    return findResult;
  }

  function generateId(){
    return Math.random(); // Temporal
  }

  function DummyDatabase(){

    this.saveNewTask = function(token, task, call, ecall) {
      setTimeout(function(){
        if(token == _token){
          task.id = generateId();
          pendingTasks.push(task);
          call(task);
        }
        else{
          ecall();
        }
      }, 150);
    };

    this.updatePendingTask = function(token, id, task, call, ecall){
      setTimeout(function(){
        if(token == _token){
          for(var i = 0; i < pendingTasks.length; i++){
            if(pendingTasks[i].id == id){
              pendingTasks[i].name = task.name;
              pendingTasks[i].description = task.description;
              pendingTasks[i].assigned = task.assigned;
              call();
            }
          }
          ecall();
        }
        else{
          ecall();
        }
      }, 150);
    };

    this.getTask = function(token, id, call, ecall){
      setTimeout(function(){
        if(token == _token){
          for(var i = 0; i < pendingTasks.length; i++){
            if(pendingTasks[i].id == id){
              call(pendingTasks[i]);
            }
          }
          for(var i = 0; i < completedTasks.length; i++){
            if(completedTasks[i].id == id){
              call(completedTasks[i]);
            }
          }
          ecall();
        }
        else{
          ecall();
        }
      }, 150);
    };
    
    this.getPendingTasks = function(token, offset, limit, call, ecall){
      setTimeout(function(){
        if(token == _token){
          call(getTasksOfList(pendingTasks, offset, limit));
        }
        else {
          ecall();
        }
      }, 150);
    };

    this.findPendingTasks = function(token, key, offset, limit, call, ecall){
      setTimeout(function(){
        if(token == _token){
          call(findTasksOnList(pendingTasks, key, offset, limit));
        }
        else{
          ecall();
        }
      }, 150);
    };

    this.getCompletedTasks = function(token, offset, limit, call, ecall){
      setTimeout(function(){
        if(token == _token){
          call(getTasksOfList(completedTasks, offset, limit));
        }
        else {
          ecall();
        }
      }, 150);
    };

    this.findCompletedTasks = function(token, key, offset, limit, call, ecall){
      setTimeout(function(){
        if(token == _token){
          call(findTasksOnList(completedTasks, key, offset, limit));
        }
        else{
          ecall();
        }
      }, 150);
    };

    this.setTaskCompleted = function(token, id, call, ecall){
      setTimeout(function(){
        if(token == _token){
          for(var i = 0; i < pendingTasks.length; i++){
            if(pendingTasks[i].id == id){
              var task = pendingTasks.splice(i, 1)[0];
              task.completed = true;
              completedTasks.push(task);
              call(); 
            }
          }
        }
        ecall();
      }, 150);
    };

  }

  return new DummyDatabase();

}

Database = createDatabaseManager();