
function createSignInManager(){

  var onSignInRequest = false;
  var uiLoaded = false;
  var signInModal = null;

  function setFormDisabled(disabled){
    document.getElementById('sign-in:form:fieldset').disabled = disabled;
    document.getElementById('sign-in:form:submit').disabled = disabled;
  }

  function showSignInUI(){
    document.getElementById('sign-in:form').reset();
    setFormDisabled(false);
    signInModal.style.display = 'block';
  }

  function hideSignInUI(){
    signInModal.style.display = 'none';
    onSignInRequest = false;
  }

  function initFormManagement(){
    var form = document.getElementById('sign-in:form');
    form.addEventListener('submit', function(evt){
      evt.preventDefault();
      setFormDisabled(true);
      Auth.signIn(form.elements['user'].value, form.elements['password'].value, function(){
        try{
          hideSignInUI();
        }catch(e){}
      }, function(){
        alert('Usuario o Contraseña incorrectos');
        setFormDisabled(false);
      });
    });
  }

  function loadSignInUI(){
    signInModal = document.createElement('div');
    signInModal.classList.add('modal-container');
    signInModal.style.display = 'none';
    Tools.httpGet('sign-in/sign-in.html', function(html){
      signInModal.innerHTML = html;
      document.body.appendChild(signInModal);
      initFormManagement();
      showSignInUI();
    });
  }

  function startSignInRequest(){
    if(signInModal){
      showSignInUI();
    }
    else {
      loadSignInUI();
    }
  }

  function SignInManager(){
  
    this.requestAuth = function(){
      if(!onSignInRequest){
        onSignInRequest = true;
        startSignInRequest();
      }
    };

  }
  return new SignInManager();
}


SignIn = createSignInManager();