(function (){
  
  var container = document.getElementById("space-container");
  var currentSpace = null;

  function loadMenu(){
    var template = document.getElementById('app:menu:template').innerHTML;
    var menuContainer = document.getElementById("spaces-menu");
    menuContainer.innerHTML = Mustache.render(template, App);
    menuContainer.addEventListener('click', function(evt){
      var idSplit = evt.target.id.split(':');
      if(idSplit.length == 3){
        if(idSplit[0] == "menu" && idSplit[1] == "element"){
          //loadSpace(idSplit[2]);
          window.location.hash = idSplit[2];
          closeMenu();
        }
      }
    });
    var showMenuButton = document.getElementById("top-nav:show-menu");
    showMenuButton.addEventListener('click', function(evt){
      openMenu();
    });
    var leftMenuButton = document.getElementById('menu:left-button');
    leftMenuButton.addEventListener('click',  function(evt){
      closeMenu();
    });
    var signOutButton = document.getElementById('menu:sign-out-button');
    signOutButton.addEventListener('click', function(evt){
      Auth.signOut();
    });
  }

  function openMenu(){
    var menuContainer = document.getElementById("spaces-menu");
    menuContainer.classList.remove('menu-right-to-left');
    menuContainer.classList.add('menu-left-to-right');
  }

  function closeMenu(){
    var menuContainer = document.getElementById("spaces-menu");
    menuContainer.classList.remove('menu-left-to-right');
    menuContainer.classList.add('menu-right-to-left');
  }

  function findSpace(id){
    for(var i = 0; i < App.Routing.length; i++)
      if(App.Routing[i].id == id) return App.Routing[i];
    console.log('space "' + id + '" Not Found');
    return null;
  }

  function findMenuIndex(id){
    for(var i = 0; i < App.Menu.length; i++)
      if(App.Menu[i].id == id) return i;
    return -1;
  }

  function unsetMenu(id){
    if(findMenuIndex(id) != -1){

    }
  }

  function setMenu(id){
    if(findMenuIndex(id) != -1){
      
    }
  }

  function loadSpace(id, args){
    var space = findSpace(id);

    if(space){
      if(currentSpace){
        if(currentSpace.id == space.id) return;
      }
      Tools.httpGet(space.manager.template, function(html){
        if(currentSpace){
          unsetMenu(currentSpace.id);
          currentSpace.manager.destroy();
          container.classList.remove(currentSpace.id);
        }
        container.innerHTML =  html;
        container.classList.add(space.id);
        setMenu(space.id);
        currentSpace = space;
        setTimeout(function(){
          space.manager.load(args);
        }, 0);
      });
    }
  }
  
  function analiceHash(){
    var hashSplit = window.location.hash.substring(1).split(':');
    var args = null;
    if(hashSplit.length > 1){
      args = {id : hashSplit[1]};
    }
    loadSpace(hashSplit[0], args);
  }

  window.onhashchange = function onhashChange(){
    analiceHash();
  };

  if(App.Routing.length > 0){
    loadMenu();
    if(!window.location.hash || window.location.hash == '#undefined')
      window.location.hash  = App.Routing[0].id;
    else{
      analiceHash();
    }
  }

  function RouterManager(){
    this.goToSpace = function(id, args){
      window.location.hash = id + (args ? ':' + args.id : '');
    };
  }

  

  Router = new RouterManager();
})();
