function createTools(){

  function http(m, url, d, l, e) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
        if (xhr.status >= 200 && xhr.status < 300)
          l(xhr.response);
        else if(e)
          e();
      }
    };
    xhr.open(m, url, true);
    xhr.send(d);
  }

  function MyTools(){
    
    this.httpGet = function(url, call, ecall){
      http('GET', url, null, call, ecall);
    };
    
  }

  return new MyTools();
}

Tools = createTools();