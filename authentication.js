
function createAuthManager(){

  var _user = "demouser";
  var _password = "demopass";
  var _token = "hjhshd871231319bjasdad71";

  var authListeners = [];
  var userAcess = null;

  var cUser = localStorage.getItem('App:access:user');
  if(cUser){
    userAcess = {
      user : cUser,
      token : localStorage.getItem('App:access:token')
    }
  }

  function userAccessChange(access){
    if(access == null){
      localStorage.removeItem('App:access:user');
      localStorage.removeItem('App:access:token');
    }
    else {
      localStorage.setItem('App:access:user', access.user);
      localStorage.setItem('App:access:token', access.token);
    }
    for(var i = 0; i < authListeners.length; i++)
      authListeners[i](access);
    userAcess = access;
  }

  function AuthManager(){

    this.onAuthChange = function(listener){
      authListeners.push(listener);
      listener(userAcess);
    };

    this.removeAuthListener = function(listener){
      var index = authListeners.indexOf(listener);
      if(index != -1)
        authListeners.splice(index, 1);
    };

    this.signIn = function(user, password, call, ecall){
      setTimeout(function() {
        if(user == _user && password == _password){
          call();
          userAccessChange({ user : user, token : _token });
        }
        else {
          ecall();
        }
      }, 1000);
    }

    this.signOut = function(){
      userAccessChange(null);
    }

  }

  return new AuthManager();

}

Auth = createAuthManager();